import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { stringify } from 'querystring';
import { productos, proveedor } from './products.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private proveedore: proveedor[] = [
    {
      nombre: "Logitec",
      cedula: 11111,
      direccion: "San Jose",
      numero: 333444,
      correo: "www@deee.com",
      codigo: 222
    }
  ];
  private productos:productos [] = [
    {
      proveedor: this.proveedore,
      precio:888,
      cantidad: 12,
      codigo: 12,
      nombre: "Tarjeta Madre",
      peso: 111,
      fecha_caducidad: "11-11-1",
      descripcion: "Tarjeta madre socket AM4"
    },
    {
      proveedor: this.proveedore,
      precio:888,
      cantidad: 12,
      codigo: 11,
      nombre: "SHARKOON REV200 BLACK",
      peso: 111,
      fecha_caducidad: "11-11-1",
      descripcion: "Case. Panel: Vidrio Temperado. Tamaño: Mid-Tower"
    },
    {
      proveedor: this.proveedore,
      precio:888,
      cantidad: 12,
      codigo: 11,
      nombre: "LOGITECH G203 PRODIGY",
      peso: 111,
      fecha_caducidad: "11-11-1",
      descripcion: "DPI: 8000"
    }
  ];
  constructor(
    private http: HttpClient
  ) { }
  getAll(){
    return [...this.productos];
  }
  getProduct(productId: number){
    return {
      ...this.productos.find(
        product => {
          return product.codigo === productId;
        }
      )
    };
  }
  deleteProduct(productId: number){
    this.productos = this.productos.filter(
      product => {
        return product.codigo !== productId;
      }
    );
  }
  addProduct(pprecio: number,
    pcantidad: number,
    pcodigo: number,
    pnombre: string,
    ppeso: number,
    pfecha_caducidad: string,
    pdescripcion: string){

      const product: productos = {
        proveedor: this.proveedore,
        precio: pprecio,
        cantidad: pcantidad,
        codigo: pcodigo,
        nombre: pnombre,
        peso: ppeso,
        fecha_caducidad: pfecha_caducidad,
        descripcion: pdescripcion
      }
      this.http.post('https://clasesmiercoles-49dde.firebaseio.com/addProduct.json',
        { 
          ...product, 
          id:null
        }).subscribe(() => {
          console.log('entro');
        });
      this.productos.push(product);
      
  }
  UpdateProduct(
    pprecio: number,
    pcantidad: number,
    pcodigo: number,
    pnombre: string,
    ppeso: number,
    pdescripcion: string
    ){
    let index = this.productos.map((x) => x.codigo).indexOf(pcodigo);

    this.productos[index].codigo = pcodigo;
    this.productos[index].cantidad = pcantidad;
    this.productos[index].nombre = pnombre;
    this.productos[index].peso = ppeso;
    this.productos[index].descripcion = pdescripcion;
    this.productos[index].precio = pprecio;

    console.log(this.productos);
  }
}
