
export interface productos{
    precio: number;
    cantidad: number;
    codigo: number;
    nombre: string;
    peso: number;
    fecha_caducidad: string;
    descripcion: string;
    proveedor: proveedor[];
}
export interface proveedor{
    nombre: string;
    cedula: number;
    direccion: string;
    numero: number;
    correo:string;
    codigo: number;
}
